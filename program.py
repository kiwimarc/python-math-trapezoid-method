import numpy as np
from time import time
from math import *

a = input("Enter start x-coordinat: ") # Start x-coordinat
b = input("Enter end x-coordinat: ") # End x-coordinat
expr = input("Enter function: ") # Function
p = input("Enter the decimal precision: ") # How precise it have to be

t = time() # Start time
lf = 1
f = 0
N = 1000
c = 0

a = eval(a)
b = eval(b)
p = eval(p)

while ('{:.'+str(p+2)+'f}').format(lf) != ('{:.'+str(p+2)+'f}').format(f):
        lf = f
        f = 0
        X = np.linspace(a, b, N)
        f = 0.0
        for k in range(len(X) - 1):
            x = X[k]
            y1 = eval(expr)
            x = X[k+1]
            y2 = eval(expr)
            f += 0.5 * ((X[k+1] - X[k]) * (y2 + y1))
        N += 1000
        c += 1

print('Time elapsed = {:.10f} sec'.format(time()-t))
print(('Area under the curve is: {:.'+str(p)+'f}').format(f))
print ('The code ran '+format(c)+' times before finished')
print ('There were '+format(N)+' trapezoids under the curve')
