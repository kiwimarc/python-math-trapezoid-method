# Python-math Trapezoid method

This is a school project, where our task was to make a program that could calculate an optional numerical method.

To use this program you need to install this:
Python (https://www.python.org/downloads/)
Numpy (https://docs.scipy.org/doc/numpy/user/install.html)

In this program the user are able to input min and max x-values, input how many trazoid there should be under the graph and what the mathematical function are.
You can find more about python math on this link: https://docs.python.org/2/library/math.html